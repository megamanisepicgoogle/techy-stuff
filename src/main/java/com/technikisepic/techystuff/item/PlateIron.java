package com.technikisepic.techystuff.item;

public class PlateIron extends OreDictItem {
  public static final String[] ORE_DICT = new String[] {"plateIron"};
  public PlateIron(String unlocalizedName, String registryName) {
    super(unlocalizedName, registryName, ORE_DICT);
  }
}
