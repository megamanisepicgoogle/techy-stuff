package com.technikisepic.techystuff.item;


public class WireCopper extends OreDictItem {
  public static final String[] ORE_DICT = new String[] {"wireCopper"};
  public WireCopper(String unlocalizedName, String registryName) {
    super(unlocalizedName, registryName, ORE_DICT);
  }

}
