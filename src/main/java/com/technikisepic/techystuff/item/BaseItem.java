package com.technikisepic.techystuff.item;

import com.technikisepic.techystuff.TechyStuff;
import net.minecraft.item.Item;

public abstract class BaseItem extends Item {
  public BaseItem(String unlocalizedName, String registryName) {
    setUnlocalizedName(TechyStuff.MOD_ID + "." + unlocalizedName);
    setRegistryName(registryName);
    setCreativeTab(TechyStuff.TECH_TAB);
  }
}
