package com.technikisepic.techystuff.item;
import net.minecraftforge.oredict.*;

abstract class OreDictItem extends BaseItem {
  public OreDictItem(String unlocalizedName, String registryName, String[] oredicts) {
    super(unlocalizedName, registryName);
    for (String ore : oredicts ) {
      OreDictionary.registerOre(ore, this);
    }
  }
}
