package com.technikisepic.techystuff;

import com.technikisepic.techystuff.proxy.*;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import com.technikisepic.techystuff.tabs.*;

@Mod(modid = TechyStuff.MOD_ID, name = TechyStuff.MOD_NAME,
 version = TechyStuff.MOD_VERSION)

public class TechyStuff {
  public static final String MOD_ID = "techystuff";
  public static final String MOD_NAME = "Techy Stuff";
  public static final String MOD_VERSION = "0.0.1";
  public static final String MC_VERSION = "[1.12.2]";

  public static final String CLIENT_PROXY = "com.technikisepic.techystuff.proxy.ClientProxy";
  public static final String SERVER_PROXY = "com.technikisepic.techystuff.proxy.ServerProxy";

  public static final CreativeTabs TECH_TAB = new TechTab("tabTechyStuff");

  @SidedProxy(clientSide = TechyStuff.CLIENT_PROXY, serverSide = TechyStuff.SERVER_PROXY)
  public static IProxy proxy;

  public static Logger logger;

  @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
}
