package com.technikisepic.techystuff.init;

import com.technikisepic.techystuff.TechyStuff;
import com.technikisepic.techystuff.block.Meltery;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(TechyStuff.MOD_ID)
public class Blocks {
    
    @ObjectHolder("meltery")
    public static final Meltery MELTERY = null;

}
