package com.technikisepic.techystuff.init;

import com.technikisepic.techystuff.TechyStuff;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(TechyStuff.MOD_ID)
public class Items {

    @ObjectHolder("copper_wire")
    public static final Item WIRE_COPPER = null;
    @ObjectHolder("iron_plate")
    public static final Item PLATE_IRON = null;

}
