package com.technikisepic.techystuff.tabs;

import com.technikisepic.techystuff.TechyStuff;
import com.technikisepic.techystuff.init.Items;


import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TechTab extends CreativeTabs {

    public TechTab(String name) {
        super(TechyStuff.MOD_ID + "." + name);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(Items.WIRE_COPPER);
    }

}
