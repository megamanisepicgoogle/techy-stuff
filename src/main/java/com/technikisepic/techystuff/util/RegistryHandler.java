package com.technikisepic.techystuff.util;

import com.technikisepic.techystuff.block.Meltery;
import com.technikisepic.techystuff.init.Blocks;
import com.technikisepic.techystuff.item.*;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber
public class RegistryHandler {

    @SubscribeEvent
    public static void registerItems(Register<Item> event) {
        final Item[] items = {
                new WireCopper("wireCopper", "copper_wire"),
                new PlateIron("plateIron", "iron_plate"),
                new ItemBlock(Blocks.MELTERY).setRegistryName(Blocks.MELTERY.getRegistryName())
        };

        event.getRegistry().registerAll(items);
    }
    
    @SubscribeEvent
    public static void registerBlocks(Register<Block> event) {
        final Block[] blocks = {
                new Meltery("meltery", "meltery")
        };
        
        event.getRegistry().registerAll(blocks);
    }

}
