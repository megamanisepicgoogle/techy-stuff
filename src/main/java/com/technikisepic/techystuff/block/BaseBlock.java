package com.technikisepic.techystuff.block;

import com.technikisepic.techystuff.TechyStuff;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public abstract class BaseBlock extends Block {
  public BaseBlock(String unlocalizedName, String registryName) {
    super(Material.ROCK);
    setUnlocalizedName(TechyStuff.MOD_ID + "." + unlocalizedName);
    setRegistryName(registryName);
    setCreativeTab(TechyStuff.TECH_TAB);
  }
}
