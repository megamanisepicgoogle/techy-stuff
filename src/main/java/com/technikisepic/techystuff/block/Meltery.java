package com.technikisepic.techystuff.block;

/**
 * @author Florian Warzecha
 * @version 1.0
 * @date 13 of Juli 2018
 */
public class Meltery extends OreDictBlock {
    
    public static final String[] oredicts = new String[]{"meltery"};
    
    public Meltery(String unlocalizedName, String registryName) {
        super(unlocalizedName, registryName, oredicts);
        this.setLightOpacity(0);
    }
}
