package com.technikisepic.techystuff.block;

import net.minecraftforge.oredict.OreDictionary;

abstract class OreDictBlock extends BaseBlock {
  public OreDictBlock(String unlocalizedName, String registryName, String[] oredicts) {
    super(unlocalizedName, registryName);
    for (String ore : oredicts ) {
      OreDictionary.registerOre(ore, this);
    }
  }
}
